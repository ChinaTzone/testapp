//
//  aApp.swift
//  Shared
//
//  Created by 曾霄逸 on 2022/2/25.
//

import SwiftUI

@main
struct aApp: App {
    var body: some Scene {
        WindowGroup {
            ContentView()
        }
    }
}
